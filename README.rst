=====
project, blog
=====

Quick start
-----------

1. Install pillow like this::

    pip install pillow

2. Add "project", "blog" to your INSTALLED_APPS setting like this::

    INSTALLED_APPS = [
        ...
        'project',
        'blog',
    ]

3. Include the polls URLconf in your project urls.py like this::

    path('project/', include('project.urls')),
    path('blog/', include('blog.urls')),

4. Run ``python manage.py migrate`` to create the polls models.

5. Start the development server and visit http://127.0.0.1:8000/project/, http://127.0.0.1:8000/blog/
   to create a project, blog (you'll need the Admin app enabled).

6. Visit http://127.0.0.1:8000/project/, http://127.0.0.1:8000/blog/ to participate in the project, blog.